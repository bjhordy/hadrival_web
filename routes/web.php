<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('index');
Route::get('/{page}/{category?}', [\App\Http\Controllers\HomeController::class, 'page'])->name('page');


Route::get('/productos/{category}/{subcategory}/{id}-{url}', [\App\Http\Controllers\HomeController::class, 'product_detail'])->name('product_detail');

Route::get('/publicaciones/{category}/{id}-{url}', [\App\Http\Controllers\HomeController::class, 'post_detail'])->name('post_detail');

//Auth::routes();

Route::get('/admin', [App\Http\Controllers\HomeController::class, 'index'])->name('admin');
