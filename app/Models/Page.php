<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use function PHPUnit\Framework\returnArgument;

class Page extends Model
{
    use HasFactory;

    public function menu()
    {
        return $this->hasOne(Menu::class);
    }

    /*//
    public function getTitleAttribute($value)
    {
        if(empty($value))
            return 'Red de Salud Leoncio Prado';
        else
            return $value;
    }*/

    //
    public function getTitleAreaAttribute()
    {
        $elements = explode(" ", $this->title);

        $total = count($elements); //1
        $limit = intval($total / 2); //0.5 -> 0
        //dd($elements, $total, $limit);

        if($limit === 0)
            return '<span>' . $this->title . '</span> ';
        else
            return '<span>' . implode(' ', array_slice($elements, 0, $limit)) . '</span> ' . implode(' ', array_slice($elements, $limit, $total));
    }

    //
    public function getSubTitleAttribute($value)
    {
        if($this->title === 'Hadrival')
            return 'Un aliado para grandes pasos';
        else
            return 'Hadrival';
    }

    //
    public function getUrlAttribute($value)
    {
        return env('APP_URL') . '/' . $value;
    }
}
