<!--====== Start Template Header ======-->
<header class="template-header sticky-header header-two">
    <div class="header-topbar d-none d-md-block">
        <div class="container-fluid container-1400">
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-4">
                    <a href="{{ url('/cotizar') }}"><i class="fa fa-receipt"></i> ¡Cotizar ahora!</a>
                    <!--<ul class="topbar-menu">
                        <li><a href="#">¡Cotizar ahora!</a></li>
                        <li><a href="gallery.html">Gallery</a></li>
                        <li><a href="about.html">About Us</a></li>
                    </ul>-->
                </div>
                <div class="col-lg-8">
                    <ul class="contact-info">
                        <li>
                            <a href="mailto:{{ $config->email_sales }}"><i class="far fa-envelope"></i> Ventas : {{ $config->email_sales }}</a>
                        </li>
                        <li>
                            <a href="tel:+51{{ $config->phone_number }}"><i class="far fa-phone"></i> Llámanos : {{ $config->phone_number }}</a>
                        </li>
                        <li>
                            <div class="social-icons">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                                <a href="#"><i class="fab fa-twitter"></i></a>
                                <a href="#"><i class="fab fa-youtube"></i></a>
                                <a href="#"><i class="fab fa-linkedin-in"></i></a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid container-1400">
        <div class="header-navigation">
            <div class="header-left">
                <div class="site-logo">
                    <a href="{{ url('/') }}">
                        <img src="{{ asset($config->logo) }}" alt="Seeva">
                    </a>
                </div>
                <nav class="site-menu menu-gap-left d-none d-xl-block">
                    <ul class="primary-menu">
                        @foreach($menus as $menu)
                            @if($menu->submenus->count() <= 0)
                                <li @if($current_page['url'] === $menu->url) class="active" @endif>
                                    <a href="{{ $menu->url }}">{{ $menu->title }}</a>
                                </li>
                            @else
                                <li>
                                    <a style="cursor: pointer">{{ $menu->title }}</a>
                                    <ul class="sub-menu">
                                        @foreach($menu->submenus as $submenu)
                                            @if($submenu->submenus->count() <= 0)
                                                <li @if($current_page['url'] === $submenu->url) class="active" @endif>
                                                    <a href="{{ $submenu->url }}">{{ $submenu->title }}</a>
                                                </li>
                                            @else
                                                <li>
                                                    <a style="cursor: pointer">{{ $submenu->title }}</a>
                                                    <ul class="sub-menu">
                                                        @foreach($submenu->submenus as $subsubmenu)
                                                            <li><a href="{{ $subsubmenu->url }}">{{ $subsubmenu->title }}</a></li>
                                                        @endforeach
                                                    </ul>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </nav>
            </div>
            <div class="header-right">
                <ul class="extra-icons">
                    <li class="d-none d-sm-block">
                        <div class="header-search-area">
                            <form action="#">
                                <input type="search" placeholder="Buscar producto">
                                <button type="submit"><i class="far fa-search"></i></button>
                            </form>
                        </div>
                    </li>
                    <li class="d-none d-xl-block">
                        <div class="off-canvas-btn style-two">
                            <span></span>
                        </div>
                    </li>
                    <li class="d-xl-none">
                        <a href="#" class="navbar-toggler">
                            <span></span>
                            <span></span>
                            <span></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Start Off Canvas -->
    <div class="slide-panel off-canvas-panel">
        <div class="panel-overlay"></div>
        <div class="panel-inner">
            <div class="canvas-logo">
                <img src="{{ asset($config->logo) }}" alt="Hadrival">
            </div>
            <div class="about-us">
                <h5 class="canvas-widget-title">Acerca de nosotros</h5>
                <p class="text-justify">{{ $config->about_us }}</p>
            </div>
            <div class="contact-us">
                <h5 class="canvas-widget-title">Contáctenos</h5>
                <ul>
                    <li>
                        <i class="far fa-map-marker-alt"></i>
                        {{ $config->address }}
                    </li>
                    <li>
                        <i class="far fa-envelope-open"></i>
                        <a href="mailto:{{ $config->email_support }}">{{ $config->email_support }}</a>
                        <a href="mailto:{{ $config->email_sales }}">{{ $config->email_sales }}</a>
                    </li>
                    <li>
                        <i class="far fa-phone"></i>
                        <a href="tel:+51{{ $config->phone_number }}">{{ $config->phone_number }}</a><br>
                        <a href="tel:+51{{ $config->mobile_number }}">{{ $config->mobile_number }}</a>
                    </li>
                </ul>
            </div>
            <a href="#" class="panel-close">
                <i class="fal fa-times"></i>
            </a>
        </div>
    </div>
    <!-- End Off Canvas -->

    <!-- Start Mobile Panel -->
    <div class="slide-panel mobile-slide-panel">
        <div class="panel-overlay"></div>
        <div class="panel-inner">
            <div class="panel-logo">
                <img src="{{ asset($config->logo) }}" alt="">
            </div>
            <nav class="mobile-menu">
                <ul class="primary-menu">
                    @foreach($menus as $menu)
                        @if($menu->submenus->count() <= 0)
                            <li @if($current_page['url'] === $menu->url) class="active" @endif>
                                <a href="{{ $menu->url }}">{{ $menu->title }}</a>
                            </li>
                        @else
                            <li>
                                <a style="cursor: pointer">{{ $menu->title }}</a>
                                <ul class="sub-menu">
                                    @foreach($menu->submenus as $submenu)
                                        @if($submenu->submenus->count() <= 0)
                                            <li @if($current_page['url'] === $submenu->url) class="active" @endif>
                                                <a href="{{ $submenu->url }}">{{ $submenu->title }}</a>
                                            </li>
                                        @else
                                            <li>
                                                <a style="cursor: pointer">{{ $submenu->title }}</a>
                                                <ul class="sub-menu">
                                                    @foreach($submenu->submenus as $subsubmenu)
                                                        <li><a href="{{ $subsubmenu->url }}">{{ $subsubmenu->title }}</a></li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </nav>
            <a href="#" class="panel-close">
                <i class="fal fa-times"></i>
            </a>
        </div>
    </div>
    <!-- Start Mobile Panel -->
</header>
<!--====== End Template Header ======-->
