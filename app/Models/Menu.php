<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;

    public function page()
    {
        return $this->belongsTo(Page::class);
    }

    public function parent()
    {
        return $this->belongsTo(Menu::class, 'parent_id');
    }

    //
    public function getUrlAttribute($value)
    {
        if(!$this->page)
            return $value;
        else
            return $this->page->url;
    }

    //
    public function getSubmenusAttribute()
    {
        $submenus = $this->where('parent_id', '=', $this->id)->get();

        return $submenus ? $submenus : false;
    }
}
