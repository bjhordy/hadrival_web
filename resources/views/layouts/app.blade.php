<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $current_page['title'] }}</title>

    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="img/png" />
    <!--====== Animate Css ======-->
    <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}" />
    <!--====== Bootstrap css ======-->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" />
    <!--====== Slick Slider ======-->
    <link rel="stylesheet" href="{{ asset('css/slick.min.css') }}" />
    <!--====== Nice Select ======-->
    <link rel="stylesheet" href="{{ asset('css/nice-select.min.css') }}" />
    <!--====== Magnific Popup ======-->
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.min.css') }}" />
    <!--====== Font Awesome ======-->
    <link rel="stylesheet" href="{{ asset('fonts/fontawesome/css/all.min.css') }}" />
    <!--====== Flaticon ======-->
    <link rel="stylesheet" href="{{ asset('fonts/flaticon/css/flaticon.css') }}" />
    <!--====== Main Css ======-->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
</head>

<body>
<div id="app">
    <!--====== Start Preloader ======-->
    <div id="preloader">
        <div id="loading-center">
            <div id="object"></div>
        </div>
    </div>
    <!--====== End Preloader ======-->

    @include('partials.header')

    @yield('content')

    <!--====== Back to Top Start ======-->
    <a class="back-to-top" href="#">
        <i class="far fa-angle-up"></i>
    </a>
    <!--====== Back to Top End ======-->

    @include('partials.footer')
</div>

<!--====== Jquery ======-->
<script src="{{ asset('js/jquery-3.6.0.min.js') }}"></script>
<!--====== Bootstrap ======-->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<!--====== Slick slider ======-->
<script src="{{ asset('js/slick.min.js') }}"></script>
<!--====== Isotope ======-->
<script src="{{ asset('js/isotope.pkgd.min.js') }}"></script>
<!--====== Images loaded ======-->
<script src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>
<!--====== In-view ======-->
<script src="{{ asset('js/jquery.inview.min.js') }}"></script>
<!--====== Nice Select ======-->
<script src="{{ asset('js/jquery.nice-select.min.js') }}"></script>
<!--====== Magnific Popup ======-->
<script src="{{ asset('js/magnific-popup.min.js') }}"></script>
<!--====== WOW Js ======-->
<script src="{{ asset('js/wow.min.js') }}"></script>
<!--====== Main JS ======-->
<script src="{{ url('js/app.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
</body>

</html>
