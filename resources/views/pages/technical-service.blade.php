@extends('layouts.app')

@section('content')
    <!--====== Page Title Start ======-->
    <section class="page-title-area page-title-bg" style="background-image: url(assets/img/section-bg/page-title.jpg);">
        <div class="container">
            <h1 class="page-title">Servicio Técnico</h1>

            <ul class="breadcrumb-nav">
                <li><a href="{{ url('/') }}">Inicio</a></li>
                <li><i class="fas fa-angle-right"></i></li>
                <li>Servicio Técnico</li>
            </ul>
        </div>
    </section>
    <!--====== Page Title End ======-->

    <!--====== Appointment Section Start ======-->
    <section class="appointment-section section-gap"> <!--section-gap-->
        <div class="appointment-form-three bg-color-secondary">
            <div class="appointment-image" style="background-image: url(assets/img/appointment/04.jpg);">
            </div>
            <div class="form-wrap">
                <div class="section-heading text-center heading-white mb-60">
                    {{--<span class="text-uppercase tagline">Make an Appointment</span>--}}
                    <h2 class="title">Servicio Técnico Post-Venta</h2>
                </div>
                <form action="#">
                    <div class="form-group">
                        <div class="input-field">
                            <label for="name">Nombres y Apellidos</label>
                            <input type="text" placeholder="" id="name">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-field">
                                <label for="number">Telefóno / Celular</label>
                                <input type="text" placeholder="" id="number">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-field">
                                <label for="email">Correo eletrónico</label>
                                <input type="email" placeholder="" id="email">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-field">
                                <label for="entity">Entidad (negocio, institución, etc.)</label>
                                <input type="text" placeholder="" id="entity">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-field">
                                <label for="job_title">Cargo</label>
                                <input type="text" placeholder="" id="job_title">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-field">
                                <label for="brand">Marca</label>
                                <input type="text" placeholder="" id="brand">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-field">
                                <label for="model">Modelo</label>
                                <input type="text" placeholder="" id="model">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-field">
                                <label for="serie">Serie</label>
                                <input type="text" placeholder="" id="serie">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-field">
                            <label for="message">Mensaje</label>
                            <textarea id="message" placeholder=""></textarea>
                        </div>
                    </div>
                    <div class=form-group">
                        <div class="input-field">
                            <button type="submit" class="template-btn template-btn-primary">
                                Solicitar soporte <i class="far fa-paper-plane"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!--====== Appointment Section End ======-->



    <!--====== Service Area Start ======-->
    <section class="services-area section-gap-top-less bg-color-grey">
        <div class="container">
            <div class="row justify-content-center service-loop">
                <div class="col-lg-4 col-md-6 col-sm-8">
                    <div class="fancy-content-box-two no-shadow mt-30">
                        <div class="thumbnail">
                            <img src="assets/img/iconic-box/05.jpg" alt="Image">
                        </div>
                        <div class="box-content">
                            <div class="icon">
                                <img src="assets/img/icon/heart-2.png" alt="">
                            </div>
                            <div class="content">
                                <h4 class="title"><a href="service-details.html">Cardiology</a></h4>
                                <p>Amet consect ascnge  eiusmod tempors</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-8">
                    <div class="fancy-content-box-two no-shadow mt-30">
                        <div class="thumbnail">
                            <img src="assets/img/iconic-box/06.jpg" alt="Image">
                        </div>
                        <div class="box-content">
                            <div class="icon">
                                <img src="assets/img/icon/brain-2.png" alt="">
                            </div>
                            <div class="content">
                                <h4 class="title"><a href="service-details.html">Neurology</a></h4>
                                <p>Quis autem reprehe nderit voluptate</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-8">
                    <div class="fancy-content-box-two no-shadow mt-30">
                        <div class="thumbnail">
                            <img src="assets/img/iconic-box/07.jpg" alt="Image">
                        </div>
                        <div class="box-content">
                            <div class="icon">
                                <img src="assets/img/icon/stomach-2.png" alt="">
                            </div>
                            <div class="content">
                                <h4 class="title"><a href="service-details.html">Orthopedics</a></h4>
                                <p>Amet consect ascnge  eiusmod tempors</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-8">
                    <div class="fancy-content-box-two no-shadow mt-30">
                        <div class="thumbnail">
                            <img src="assets/img/iconic-box/08.jpg" alt="Image">
                        </div>
                        <div class="box-content">
                            <div class="icon">
                                <img src="assets/img/icon/virus-2.png" alt="">
                            </div>
                            <div class="content">
                                <h4 class="title"><a href="service-details.html">Covid 19</a></h4>
                                <p>Quis autem reprehe nderit voluptate</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-8">
                    <div class="fancy-content-box-two no-shadow mt-30">
                        <div class="thumbnail">
                            <img src="assets/img/iconic-box/09.jpg" alt="Image">
                        </div>
                        <div class="box-content">
                            <div class="icon">
                                <img src="assets/img/icon/bronchus.png" alt="">
                            </div>
                            <div class="content">
                                <h4 class="title"><a href="service-details.html">Orthopedics</a></h4>
                                <p>Quis autem reprehe nderit voluptate</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-8">
                    <div class="fancy-content-box-two no-shadow mt-30">
                        <div class="thumbnail">
                            <img src="assets/img/iconic-box/10.jpg" alt="Image">
                        </div>
                        <div class="box-content">
                            <div class="icon">
                                <img src="assets/img/icon/lungs.png" alt="">
                            </div>
                            <div class="content">
                                <h4 class="title"><a href="service-details.html">Pulmonary</a></h4>
                                <p>Quis autem reprehe nderit voluptate</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== Service Area End ======-->

    <!--====== Feature Section Start ======-->
    <section class="feature-section section-gap">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-xl-7 col-lg-6 order-lg-last">
                    <div class="feature-img text-center text-lg-right">
                        <img src="assets/img/section-img/feature-img.jpg" alt="Image">
                    </div>
                </div>
                <div class="col-xl-5 col-lg-6 col-md-10">
                    <div class="feature-text mt-md-50">
                        <div class="section-heading mb-30">
                            <span class="tagline">How Can We Help</span>
                            <h2 class="title">Flexible & Responsive to Changing Needs</h2>
                        </div>
                        <p>
                            Sed ut perspiciatis unde omnis iste natus error voluptatem accusantium doloremque laudantium totam rem aperieaqueys epsa quae abillo inventore veritatis et quase
                        </p>
                        <div class="row">
                            <div class="col-lg-5 col-sm-6">
                                <div class="simple-icon mt-40 wow fadeInUp" data-wow-delay="0.3s">
                                    <div class="icon">
                                        <i class="flaticon-stethoscope"></i>
                                    </div>
                                    <h4 class="title">Professional Doctors</h4>
                                    <p>Sed perspiciatis unde omnis natus error</p>
                                </div>
                            </div>
                            <div class="col-lg-5 col-sm-6">
                                <div class="simple-icon mt-40 wow fadeInUp" data-wow-delay="0.4s">
                                    <div class="icon">
                                        <i class="flaticon-checkup"></i>
                                    </div>
                                    <h4 class="title">Very Friendly Environment</h4>
                                    <p>Sed perspiciatis unde omnis natus error</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== Feature Section End ======-->
@endsection
