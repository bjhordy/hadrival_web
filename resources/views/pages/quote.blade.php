@extends('layouts.app')

@section('content')
    <!--====== Page Title Start ======-->
    <section class="page-title-area page-title-bg" style="background-image: url({{ asset('img/section-bg/hadrival-quote-bg.png') }});">
        <div class="container">
            <h1 class="page-title">Cotización</h1>

            <!--<ul class="breadcrumb-nav">
                <li><a href="#">Home</a></li>
                <li><i class="fas fa-angle-right"></i></li>
                <li>Services</li>
            </ul>-->
        </div>
    </section>
    <!--====== Page Title End ======-->

    <!--====== Appointment Section Start ======-->
    <section class="appointment-section section-gap">
        <div class="appointment-form-two">
            <div class="form-wrap">
                <div class="section-heading mb-40">
                    {{--<span class="text-uppercase tagline">Solicitar cotización</span>--}}
                    <h2 class="title">Cotiza para mayor información</h2>
                </div>
                <form action="#">
                    <div class="form-group">
                        <div class="input-field">
                            <label for="name">Nombres y Apellidos</label>
                            <input type="text" placeholder="" id="name">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-field">
                                <label for="number">Telefóno / Celular</label>
                                <input type="text" placeholder="" id="number">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-field">
                                <label for="email">Correo eletrónico</label>
                                <input type="email" placeholder="" id="email">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-field">
                                <label for="entity">Entidad (negocio, institución, etc.)</label>
                                <input type="text" placeholder="" id="entity">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-field">
                                <label for="job_title">Cargo</label>
                                <input type="text" placeholder="" id="job_title">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-field">
                                <label for="state">Departamento</label>
                                <select id="state"></select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-field">
                                <label for="province">Provincia</label>
                                <select id="province"></select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-field">
                                <label for="district">Distrito</label>
                                <select id="district"></select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-field">
                            <label for="specialty">Especialidad</label>
                            <select id="specialty"></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-field">
                            <label for="message">Mensaje</label>
                            <textarea id="message" placeholder=""></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-field">
                            <button type="submit" class="template-btn">
                                Solicitar cotización <i class="far fa-paper-plane"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="appointment-image" style="background-image: url(assets/img/appointment/07.jpg);">
            </div>
        </div>
    </section>
    <!--====== Appointment Section End ======-->
@endsection
