<!--====== Start Template Footer ======-->
<footer class="template-footer template-footer-white">
    <div class="footer-inner bg-color-primary">
        <div class="container">
            <div class="footer-widgets">
                <div class="row">
                    <div class="col-lg-3 col-md-8">
                        <div class="widget text-widget">
                            <div class="footer-logo">
                                <img src="{{ asset($config->logo_white) }}" alt="Hadrival">
                            </div>
                            <p class="text-justify">{{ $config->about_us }}</p>
                            <ul class="contact-list">
                                <li>
                                    <span><i class="far fa-map-marker-alt"></i>{{ $config->address }}</span>
                                </li>
                                <li>
                                    <a href="mailto:{{ $config->email_support }}"><i class="far fa-envelope"></i>{{ $config->email_support }}</a>
                                </li>
                                <li>
                                    <a href="tel:+51{{ $config->phone_number }}"><i class="far fa-phone"></i>{{ $config->phone_number }}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-xl-5 col-md-6">
                                <div class="widget nav-widget">
                                    <h4 class="widget-title">Sobre Nosotros</h4>
                                    <ul class="nav-links">
                                        <li><a href="#">Quienes somos</a></li>
                                        <li><a href="#">Nuestras Marcas</a></li>
                                        <li><a href="#">Preguntas Frecuentes</a></li>
                                        <li><a href="#">Trabaja con nosotros</a></li>
                                        <li><a href="#">Facturas Electrónicas</a></li>
                                        <li><a href="#">Políticas de calidad</a></li>
                                        <li><a href="#">Contacto</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xl-7 col-md-6">
                                <div class="widget instagram-widget">
                                    <h4 class="widget-title">Algunos productos</h4>
                                    <div class="instagram-images">
                                        <div class="single-image">
                                            <img src="assets/img/instagram/01.jpg" alt="Instagram">
                                            <a href="#"><i class="fab fa-instagram"></i></a>
                                        </div>
                                        <div class="single-image">
                                            <img src="assets/img/instagram/02.jpg" alt="Instagram">
                                            <a href="#"><i class="fab fa-instagram"></i></a>
                                        </div>
                                        <div class="single-image">
                                            <img src="assets/img/instagram/03.jpg" alt="Instagram">
                                            <a href="#"><i class="fab fa-instagram"></i></a>
                                        </div>
                                        <div class="single-image">
                                            <img src="assets/img/instagram/04.jpg" alt="Instagram">
                                            <a href="#"><i class="fab fa-instagram"></i></a>
                                        </div>
                                        <div class="single-image">
                                            <img src="assets/img/instagram/05.jpg" alt="Instagram">
                                            <a href="#"><i class="fab fa-instagram"></i></a>
                                        </div>
                                        <div class="single-image">
                                            <img src="assets/img/instagram/06.jpg" alt="Instagram">
                                            <a href="#"><i class="fab fa-instagram"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-10">
                        <div class="widget newsletters-widget">
                            <h4 class="widget-title">Suscríbete</h4>
                            <p class="text-justify">
                                Entérate de todas las novedades sobre nuestros productos médicos, uniformes y servicios.
                            </p>
                            <form action="#" class="newsletters-form">
                                <input type="email" placeholder="Correo electrónico">
                                <button type="submit"><i class="far fa-arrow-right"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright-area">
                <p>© @if(date("Y")>2022){{"2022-".date("Y")}}@else {{"2022"}}@endif Hadrival. Todos los derechos reservados | Desarrollado con <i class="fa fa-heart"></i> por <a href="https://www.devando.pe" target="_blank">Devando</a></p>
            </div>
        </div>
    </div>
</footer>
<!--====== End Template Footer ======-->
