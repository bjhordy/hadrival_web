<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use HasFactory, SoftDeletes;

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    public function attachments()
    {
        return $this->hasMany(Attachment::class);
    }

    public function scopePublished($query)
    {
        return $query->where('published_at', '<=', now())->where('draft', '=', false);
    }

    public function getCoverImageAttribute($value)
    {
        return asset($value);
    }

    public function getLinkAttribute()
    {
        return env('APP_URL') . '/publicaciones/' . $this->category->url . '/' . $this->id . '-' . $this->url;
    }

    public function getPublishedAtAttribute($value)
    {
        return Carbon::parse($value)->diffForHumans();
    }

   /* protected $dates = [
        'created_at', 'updated_at', 'deleted_at'
    ];*/

    /*protected $casts = [
        'live'  => 'boolean'
    ];*/
}
