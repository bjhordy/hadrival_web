@extends('layouts.app')

@section('content')
    <!--====== Page Title Start ======-->
    <section class="page-title-area page-title-bg" style="background-image: url({{ asset('img/section-bg/hadrival-contact-bg.png') }});">
        <div class="container">
            <h1 class="page-title">Contáctenos</h1>

            <ul class="breadcrumb-nav">
                <li><a href="{{ url('/') }}">Inicio</a></li>
                <li><i class="fas fa-angle-right"></i></li>
                <li>Contacto</li>
            </ul>
        </div>
    </section>
    <!--====== Page Title End ======-->

    <!--====== Contact Info Section Start ======-->
    <section class="section-gap contact-top-wrappper">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5 col-lg-6 col-md-10">
                    <div class="contact-info-wrapper">
                        <div class="single-contact-info">
                            <div class="single-contact-info">
                                <h3 class="info-title">
                                    <i class="fal fa-map-marker-alt"></i> Dirección
                                </h3>
                                <p>{{ $config->address }}</p>
                            </div>
                            <div class="single-contact-info">
                                <h3 class="info-title">
                                    <i class="fal fa-address-card"></i> Contacto
                                </h3>
                                <ul>
                                    <li>
                                        <span>Telefóno</span><a href="tel:+51{{ $config->phone_number }}">{{ $config->phone_number }}</a>
                                    </li>
                                    <li>
                                        <span>Celular</span><a href="tel:+51{{ $config->mobile_number }}">{{ $config->mobile_number }}</a>
                                    </li>
                                    <li>
                                        <span>Correo ventas</span><a href="mailto:{{ $config->email_sales }}">{{ $config->email_sales }}</a>
                                    </li>
                                    <li>
                                        <span>Correo soporte</span><a href="mailto:{{ $config->email_support }}">{{ $config->email_support }}</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="single-contact-info">
                                <h3 class="info-title">
                                    <i class="fal fa-comments"></i> Síguenos
                                </h3>
                                <p>Siguenos en nuestras redes sociales</p>
                                <p class="social-icon">
                                    <a href="#"><i class="fab fa-facebook"></i></a>
                                    <a href="#"><i class="fab fa-twitter-square"></i></a>
                                    <a href="#"><i class="fab fa-linkedin"></i></a>
                                    <a href="#"><i class="fab fa-youtube-square"></i></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-7 col-lg-6 col-md-10">
                    <div class="section-heading mb-60 text-center">
                        {{--<span class="text-uppercase tagline">Estemos siempre en contacto</span>--}}
                        <h2 class="title">Dejanos un mensaje</h2>
                    </div>
                    <form action="#" class="contact-form">
                        <div class="form-group">
                            <div class="input-field">
                                <label for="name">Nombres y Apellidos <sup class="">(*)</sup></label>
                                <input type="text" placeholder="" id="name">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-field">
                                    <label for="number">Telefóno / Celular</label>
                                    <input type="text" placeholder="" id="number">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-field">
                                    <label for="email">Correo eletrónico</label>
                                    <input type="email" placeholder="" id="email">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-field">
                                    <label for="entity">Entidad (negocio, institución, etc.)</label>
                                    <input type="text" placeholder="" id="entity">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-field">
                                    <label for="job_title">Cargo</label>
                                    <input type="text" placeholder="" id="job_title">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="input-field">
                                    <label for="state">Departamento</label>
                                    <select id="state"></select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-field">
                                    <label for="province">Provincia</label>
                                    <select id="province"></select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-field">
                                    <label for="district">Distrito</label>
                                    <select id="district"></select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-field">
                                <label for="specialty">Especialidad</label>
                                <select id="specialty"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-field">
                                <label for="message">Mensaje</label>
                                <textarea id="message" placeholder="Escribe aquí tu mensaje...."></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="text-center">
                                <button class="template-btn">Enviar mensaje <i class="far fa-paper-plane"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!--====== Contact Info Section End ======-->

    <!--====== Contact Form Start ======-->
    <section class="contact-form-area">
        <div class="contact-map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d107201.226767341!2d-74.05027451789393!3d40.71534534062428!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1634195102348!5m2!1sen!2sbd" loading="lazy"></iframe>
        </div>
    </section>
    <!--====== Contact Form End ======-->
@endsection
