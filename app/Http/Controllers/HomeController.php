<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Client;
use App\Models\Configuration;
use App\Models\FrequentQuestion;
use App\Models\Menu;
use App\Models\Page;
use App\Models\Post;
use App\Models\Slider;
use App\Models\Testimonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // Header
        $config = Configuration::first();
        $menus = Menu::whereNull('parent_id')->orderBy('order', 'asc')->get();
        //Content
        $sliders = Slider::orderBy('order', 'asc')->get();

        $total_statistics = [
            'establishments' => '71',
            'ambulances' => '10',
            'assistance' => '737',
            'administrative' => '118',
        ];

        $last_posts = Post::published()->whereDoesntHave('category', function ($q) {
            $q->where('url', '=', 'comunicados');
        })->orderBy('created_at', 'desc')->limit(3)->get();
        $frequent_questions = FrequentQuestion::orderBy('created_at', 'desc')->limit(5)->get();
        $testimonials = Testimonial::all();
        $brands = Brand::all();
        $clients = Client::all();
        //Footer
        $popular_posts = Post::published()->orderBy('views', 'desc')->limit(5)->get();

        // Current Page
        $page = Page::find(1);
        $popup_posts = Post::published()->where('popup', '=', true)->orderBy('published_at', 'desc')->limit(5)->get();
        $current_page = [];
        $current_page['title'] = $page->title . ' - ' . $page->subtitle;
        $current_page['description'] = $page->description;
        $current_page['tags'] = $page->tags;
        $current_page['url'] = $page->url;

        return view('index')
            ->with(compact('current_page', 'popup_posts', 'config', 'menus',
                'sliders', 'total_statistics', 'last_posts', 'frequent_questions',
                'testimonials', 'brands', 'clients', 'popular_posts'));
    }

    public function page($url, $category = null)
    {
        // Current Page
        $page = Page::where('url', '=', $url)->firstOrFail();
        $current_page = [];
        $current_page['title'] = $page->title . ' - ' . $page->subtitle;
        $current_page['description'] = $page->description;
        $current_page['tags'] = $page->tags;
        $current_page['url'] = $page->url;

        // Header
        $config = Configuration::first();
        $menus = Menu::whereNull('parent_id')->orderBy('order', 'asc')->get();
        // Footer
        $popular_posts = Post::orderBy('views', 'desc')->limit(5)->get();

        if($url === 'quienes-somos')
        {
            return view('pages.about_us')
                ->with(compact('current_page', 'page', 'config', 'menus', 'popular_posts'));
        }

        elseif($url === 'servicio-tecnico')
        {
            return view('pages.technical-service')
                ->with(compact('current_page', 'page', 'config', 'menus', 'popular_posts'));
        }

        elseif($url === 'contacto')
        {
            return view('pages.contact')
                ->with(compact('current_page', 'page', 'config', 'menus', 'popular_posts'));
        }

        elseif($url === 'cotizar')
        {
            return view('pages.quote')
                ->with(compact('current_page', 'page', 'config', 'menus', 'popular_posts'));
        }

        elseif($url === 'publicaciones')
        {
            // Content
            $category = Category::where('url', '=', $category)->first();

            if($category)
                $current_page['title'] = $category->title . ' - ' . $page->subtitle;

            // Sidebar Content
            $categories = Category::with('Posts')->get();
            $recommended_posts = Post::published()->orderBy('views', 'desc')->limit(3)->get();

            return view('posts.index')
                ->with(compact('current_page', 'config', 'menus', 'category', 'categories', 'recommended_posts', 'popular_posts'));
        }

        return view('pages.blank')
            ->with(compact('current_page', 'page', 'config', 'menus', 'popular_posts'));
    }
}
