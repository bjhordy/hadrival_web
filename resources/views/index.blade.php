@extends('layouts.app')

@section('content')
    <!--====== Hero Slider Start ======-->
    <section class="hero-slider hero-slider-one">
        <div class="hero-slider-active">
            @foreach($sliders as $slider)
                <div class="single-hero-slider">
                    <div class="hero-slider-bg bg-size-cover" style="background-image: url({{ asset($slider->image) }});"></div>
                    <div class="container container-1400">
                        <div class="slider-content-box" data-animation="fadeInUp" data-delay="0.4s">
                            {!! $slider->content !!}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="hero-slider-arrow"></div>
    </section>
    <!--====== Hero Slider End ======-->

    <!--====== About Section Start ======-->
    <section class="about-section section-gap">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-xl-7 col-lg-9 col-md-11">
                    <div class="tile-gallery mb-xl-50">
                        <div class="image-one wow fadeInLeft" data-wow-delay="0.3s">
                            <img class="animate-float-bob-x" src="{{ asset('img/tile-gallery/tile-gallery-hadrival-316-385.png') }}" alt="Image">
                        </div>
                        <div class="image-two wow fadeInDown" data-wow-delay="0.4s">
                            <img src="{{ asset('img/tile-gallery/tile-gallery-hadrival-370-513.png') }}" alt="Image">
                        </div>
                        <div class="image-three wow fadeInUp" data-wow-delay="0.5s">
                            <img src="{{ asset('img/tile-gallery/tile-gallery-hadrival-316-247.png') }}" alt="Image">
                        </div>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-8 col-md-9">
                    <div class="about-text">
                        <div class="section-heading mb-30">
                            <span class="text-uppercase tagline">Acerca de nosotros</span>
                            <h2 class="title">Las mejores marcas de equipos médicos e indumentaria corporativa</h2>
                        </div>
                        <p>
                            Brindando las mejores soluciones tecnológicas de equipamiento médico y servicio técnico de primer nivel.
                        </p>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="simple-icon text-center mt-40 wow fadeInUp" data-wow-delay="0.3s">
                                    <div class="icon">
                                        <i class="far fa-stars"></i>
                                    </div>
                                    <h4 class="title">Mejoramos tus servicios</h4>
                                    <p>Brinda una atención de calidad y profesional</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="simple-icon text-center mt-40 wow fadeInUp" data-wow-delay="0.4s">
                                    <div class="icon">
                                        <i class="far fa-tools"></i>
                                    </div>
                                    <h4 class="title">Servicio técnico al instante</h4>
                                    <p>Contamos con el mejor servicio técnico</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== About Section End ======-->

    <!--====== Feature Section Start ======-->
    <!--<section class="feature-section section-gap bg-color-grey">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-heading text-center mb-40">
                        <span class="tagline">What We Offer</span>
                        <h2 class="title">Breakthrough in Comprehensive, Flexible Care Models</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center service-looop">
                <div class="col-lg-3 col-md-6 col-sm-10">
                    <div class="iconic-box-two mt-30 wow fadeInUp" data-wow-delay="0.3s">
                        <div class="icon">
                            <i class="flaticon-tooth-1"></i>
                        </div>
                        <h4 class="title"><a href="service-details.html">Medicine Care</a></h4>
                        <p>
                            Perspiciatis unde omniste natus error volutatem
                        </p>
                        <a href="service-details.html" class="box-link"><i class="far fa-plus"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-10">
                    <div class="iconic-box-two mt-30 wow fadeInUp" data-wow-delay="0.4s">
                        <div class="icon">
                            <i class="flaticon-tooth-2"></i>
                        </div>
                        <h4 class="title"><a href="service-details.html">Medicine Care</a></h4>
                        <p>
                            Perspiciatis unde omniste natus error volutatem
                        </p>
                        <a href="service-details.html" class="box-link"><i class="far fa-plus"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-10">
                    <div class="iconic-box-two mt-30 wow fadeInUp" data-wow-delay="0.5s">
                        <div class="icon">
                            <i class="flaticon-tooth-3"></i>
                        </div>
                        <h4 class="title"><a href="service-details.html">Dental Care</a></h4>
                        <p>
                            Perspiciatis unde omniste natus error volutatem
                        </p>
                        <a href="service-details.html" class="box-link"><i class="far fa-plus"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-10">
                    <div class="iconic-box-two mt-30 wow fadeInUp" data-wow-delay="0.6s">
                        <div class="icon">
                            <i class="flaticon-tooth-4"></i>
                        </div>
                        <h4 class="title"><a href="service-details.html">Child Care</a></h4>
                        <p>
                            Perspiciatis unde omniste natus error volutatem
                        </p>
                        <a href="service-details.html" class="box-link"><i class="far fa-plus"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>-->
    <!--====== Feature Section End ======-->

    <!--====== Call to Action Start ======-->
    <section class="cta-section bg-size-cover section-gap-100 bg-color-primary blend-mode-multiply" style="background-image: url({{ asset('img/cta-img/hadrival-promo-section.png') }});">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5">
                    <div class="section-heading heading-white">
                        <span class="tagline">¡Descuentos exclusivos!</span>
                        <h2 class="title">Descubre nuestras mejores ofertas</h2>
                    </div>
                </div>
                <div class="col-lg-7">
                    <ul class="cta-buttons d-sm-flex justify-content-lg-end mt-md-40 flex-wrap">
                        <li class="mb-3 mb-sm-0"><a href="{{ url('/promociones') }}" class="template-btn template-btn-white wow fadeInRight" data-wow-delay="0.4s">Ver promociones <i class="far fa-arrow-right"></i></a></li>
                        <!--<li class="ml-sm-2"><a href="#" class="template-btn template-btn-bordered wow fadeInRight" data-wow-delay="0.5s">Get Appointment <i class="far fa-plus"></i></a></li>-->
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--====== Call to Action End ======-->

    <!--====== Product Carousel Start ======-->
    <section class="product-carousel-section section-gap">
        <div class="container">
            <div class="product-carousel-heading mb-70">
                <div class="row align-items-end">
                    <div class="col-lg-6 col-sm-8">
                        <div class="section-heading">
                            <span class="text-uppercase tagline">Nuestros Productos</span>
                            <h2 class="title">Productos destacados</h2>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-4">
                        <div class="product-carousel-arrows"></div>
                    </div>
                </div>
            </div>
            <div class="row product-carousel">
                <div class="col-lg-3">
                    <div class="product-box product-box-bg">
                        <div class="brand text-center">
                            <img src="{{ asset('img/brand-logos/ge-healthcare-logo.png') }}" height="36px" alt="GE Healthcare">
                        </div>
                        <div class="thumbnail">
                            <img src="assets/img/products/11.png" alt="Image">
                        </div>
                        <div class="content">
                            <h5 class="title">
                                <a href="product-details.html">Carescape B850</a>
                            </h5>
                        </div>
                        <div class="hover-action">
                            <a href="#" class="add-to-cart">Cotizar <i class="far fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="product-box product-box-bg">
                        <div class="brand text-center">
                            <img src="{{ asset('img/brand-logos/zeiss-logo.png') }}" height="36px" alt="Zeiss">
                        </div>
                        <div class="thumbnail">
                            <img src="assets/img/products/11.png" alt="Image">
                        </div>
                        <div class="content">
                            <h5 class="title">
                                <a href="product-details.html">Cirrus 6000</a>
                            </h5>
                        </div>
                        <div class="hover-action">
                            <a href="#" class="add-to-cart">Cotizar <i class="far fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="product-box product-box-bg">
                        <div class="brand text-center">
                            <img src="{{ asset('img/brand-logos/elekta-logo.png') }}" height="36px" alt="Elekta">
                        </div>
                        <div class="thumbnail">
                            <img src="assets/img/products/11.png" alt="Elekta">
                        </div>
                        <div class="content">
                            <h5 class="title">
                                <a href="product-details.html">Versa HD</a>
                            </h5>
                        </div>
                        <div class="hover-action">
                            <a href="#" class="add-to-cart">Cotizar <i class="far fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="product-box product-box-bg">
                        <div class="brand text-center">
                            <img src="{{ asset('img/brand-logos/stryker-logo.png') }}" height="36px" alt="Stryker">
                        </div>
                        <div class="thumbnail">
                            <img src="assets/img/products/11.png" alt="Stryker">
                        </div>
                        <div class="content">
                            <h5 class="title">
                                <a href="product-details.html">Consola 1688 AIM</a>
                            </h5>
                        </div>
                        <div class="hover-action">
                            <a href="#" class="add-to-cart">Cotizar <i class="far fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="product-box product-box-bg">
                        <div class="brand text-center">
                            <img src="{{ asset('img/brand-logos/merivaara-logo.png') }}" height="36px" alt="Merivaara">
                        </div>
                        <div class="thumbnail">
                            <img src="assets/img/products/11.png" alt="Merivaara">
                        </div>
                        <div class="content">
                            <h5 class="title">
                                <a href="product-details.html">Grand Promerix</a>
                            </h5>
                        </div>
                        <div class="hover-action">
                            <a href="#" class="add-to-cart">Cotizar <i class="far fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="product-box product-box-bg">
                        <div class="brand text-center">
                            <img src="{{ asset('img/brand-logos/masimo-logo.png') }}" height="36px" alt="Masimo">
                        </div>
                        <div class="thumbnail">
                            <img src="assets/img/products/11.png" alt="Masimo">
                        </div>
                        <div class="content">
                            <h5 class="title">
                                <a href="product-details.html">Radical 7</a>
                            </h5>
                        </div>
                        <div class="hover-action">
                            <a href="#" class="add-to-cart">Cotizar <i class="far fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== Product Carousel End ======-->

    <!--====== Counter Section Start ======-->
    <!--<section class="counter-section">
        <div class="container">
            <div class="counter-inner-two mb-negative">
                <div class="row justify-content-between">
                    <div class="col-lg-auto col-md-5 col-sm-6">
                        <div class="counter-item counter-white">
                            <div class="icon">
                                <i class="flaticon-stethoscope"></i>
                            </div>
                            <div class="counter-wrap">
                                <span class="counter">359</span>
                                <span class="suffix">+</span>
                            </div>
                            <h6 class="title">Professional Doctors</h6>
                        </div>
                    </div>
                    <div class="col-lg-auto col-md-5 col-sm-6">
                        <div class="counter-item counter-white">
                            <div class="icon">
                                <i class="flaticon-stethoscope"></i>
                            </div>
                            <div class="counter-wrap">
                                <span class="counter">85</span>
                                <span class="suffix">k+</span>
                            </div>
                            <h6 class="title">Saticfied Our Clients</h6>
                        </div>
                    </div>
                    <div class="col-lg-auto col-md-5 col-sm-6">
                        <div class="counter-item counter-white">
                            <div class="icon">
                                <i class="flaticon-stethoscope"></i>
                            </div>
                            <div class="counter-wrap">
                                <span class="counter">863</span>
                                <span class="suffix">+</span>
                            </div>
                            <h6 class="title">Win International Awards</h6>
                        </div>
                    </div>
                    <div class="col-lg-auto col-md-5 col-sm-6">
                        <div class="counter-item counter-white">
                            <div class="icon">
                                <i class="flaticon-stethoscope"></i>
                            </div>
                            <div class="counter-wrap">
                                <span class="counter">86</span>
                                <span class="suffix">k+</span>
                            </div>
                            <h6 class="title">4.9 Star Reviews</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>-->
    <!--====== Counter Section End ======-->

    <!--====== Video Section Start ======-->
    <section class="video-section">
        <div class="video-with-content bg-color-primary">
            <div class="seeva-video">
                <div class="video-thumbnail">
                    <img src="{{ asset('img/section-img/hadrival-sonoscape-video.png') }}" alt="Image">
                </div>
                <a href="https://www.youtube.com/watch?v=Ud1MCYRQv58" class="video-popup"><i class="fas fa-play"></i></a>
            </div>

            <div class="container">
                <div class="row justify-content-lg-end justify-content-center">
                    <div class="col-lg-5 col-md-9">
                        <div class="video-content">
                            <div class="section-heading mb-20">
                                <span class="tagline">Inteligencia Articulada</span>
                                <h2 class="title">Última y destacada plataforma Wis+ de SonoScape</h2>
                            </div>
                            <p class="text-justify">
                                Está diseñado para proporcionar pruebas más perspicaces y constructivas para el diagnóstico a través de la visualización de detalles auténticos, un análisis inteligente fácil pero eficaz y un flujo de trabajo racionalizado.
                            </p>
                            <div class="row">
                                <div class="col-6">
                                    <ul class="check-list mt-35 pr-xl-4">
                                        <li class="wow fadeInUp" data-wow-delay="0.3s">
                                            Wis+
                                        </li>
                                        <li class="wow fadeInUp" data-wow-delay="0.4s">
                                            Inteligencia Artificial
                                        </li>
                                        <li class="wow fadeInUp" data-wow-delay="0.4s">
                                            S-Fetus
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-6">
                                    <ul class="check-list mt-35 pr-xl-4">
                                        <li class="wow fadeInUp" data-wow-delay="0.3s">
                                            S-Thyroid
                                        </li>
                                        <li class="wow fadeInUp" data-wow-delay="0.4s">
                                            CEUS
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <a href="{{ url('/cotizar') }}" class="template-btn template-btn-bordered mt-40 wow fadeInUp" data-wow-delay="0.5s">¡Cotiza aquí! <i class="far fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== Video Section End ======-->

    <!--====== Big Tagline Start ======-->
    <section class="big-tagline">
        <div class="container-fluid">
            <h2 class="tagline">El servicio técnico de todos nuestros equipos son realizados con repuestos originales.</h2>
        </div>
    </section>
    <!--====== Big Tagline End ======-->

    <!--====== Gallery Section Start ======-->
    <section class="gallery-section section-gap-top">
        <div class="container-fluid fluid-70">
            <div class="section-heading text-center mb-40">
                <span class="text-uppercase tagline">Nuestra Galería</span>
                <h2 class="title">Instalaciones, mantenimiento y reparaciones</h2>
            </div>
            <div class="row gallery-loop justify-content-center">
                <div class="col-xl-3 col-lg-4 col-sm-6">
                    <div class="gallery-item-one mt-30">
                        <div class="gallery-thumbnail">
                            <img src="assets/img/gallery/01.jpg" alt="Image">
                        </div>
                        <div class="gallery-caption">
                            <div>
                                <a href="assets/img/gallery/01.jpg" class="plus-icon"></a>
                                <h3 class="title"><a href="#">Medical Doctors</a></h3>
                                <p>259+ Doctors Available</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-sm-6">
                    <div class="gallery-item-one mt-30">
                        <div class="gallery-thumbnail">
                            <img src="assets/img/gallery/02.jpg" alt="Image">
                        </div>
                        <div class="gallery-caption">
                            <div>
                                <a href="assets/img/gallery/02.jpg" class="plus-icon"></a>
                                <h3 class="title"><a href="#">Medical Doctors</a></h3>
                                <p>259+ Doctors Available</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-sm-6">
                    <div class="gallery-item-one mt-30">
                        <div class="gallery-thumbnail">
                            <img src="assets/img/gallery/03.jpg" alt="Image">
                        </div>
                        <div class="gallery-caption">
                            <div>
                                <a href="assets/img/gallery/03.jpg" class="plus-icon"></a>
                                <h3 class="title"><a href="#">Medical Doctors</a></h3>
                                <p>259+ Doctors Available</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-sm-6">
                    <div class="gallery-item-one mt-30">
                        <div class="gallery-thumbnail">
                            <img src="assets/img/gallery/04.jpg" alt="Image">
                        </div>
                        <div class="gallery-caption">
                            <div>
                                <a href="assets/img/gallery/04.jpg" class="plus-icon"></a>
                                <h3 class="title"><a href="#">Medical Doctors</a></h3>
                                <p>259+ Doctors Available</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-sm-6">
                    <div class="gallery-item-one mt-30">
                        <div class="gallery-thumbnail">
                            <img src="assets/img/gallery/05.jpg" alt="Image">
                        </div>
                        <div class="gallery-caption">
                            <div>
                                <a href="assets/img/gallery/05.jpg" class="plus-icon"></a>
                                <h3 class="title"><a href="#">Medical Doctors</a></h3>
                                <p>259+ Doctors Available</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-sm-6">
                    <div class="gallery-item-one mt-30">
                        <div class="gallery-thumbnail">
                            <img src="assets/img/gallery/06.jpg" alt="Image">
                        </div>
                        <div class="gallery-caption">
                            <div>
                                <a href="assets/img/gallery/06.jpg" class="plus-icon"></a>
                                <h3 class="title"><a href="#">Medical Doctors</a></h3>
                                <p>259+ Doctors Available</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-sm-6">
                    <div class="gallery-item-one mt-30">
                        <div class="gallery-thumbnail">
                            <img src="assets/img/gallery/07.jpg" alt="Image">
                        </div>
                        <div class="gallery-caption">
                            <div>
                                <a href="assets/img/gallery/07.jpg" class="plus-icon"></a>
                                <h3 class="title"><a href="#">Medical Doctors</a></h3>
                                <p>259+ Doctors Available</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-sm-6">
                    <div class="gallery-item-one mt-30">
                        <div class="gallery-thumbnail">
                            <img src="assets/img/gallery/08.jpg" alt="Image">
                        </div>
                        <div class="gallery-caption">
                            <div>
                                <a href="assets/img/gallery/08.jpg" class="plus-icon"></a>
                                <h3 class="title"><a href="#">Medical Doctors</a></h3>
                                <p>259+ Doctors Available</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== Gallery Section End ======-->

    <!--====== Service Section Start ======-->
    <section class="services-section section-gap">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-7 col-md-10">
                    <div class="section-heading text-center mb-40">
                        <span class="text-uppercase tagline">Nuestras Especialidades</span>
                        <h2 class="title">Los servicios más destacados</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid fluid-100">
            <div class="row justify-content-center custom-service-grid">
                <div class="col">
                    <div class="fancy-content-box mt-30">
                        <div class="box-thumbnail">
                            <img src="{{ asset('img/fancy-box/hadrival-consultorio-externo.png') }}" alt="Image">
                        </div>
                        <div class="box-content">
                            <h4 class="title"><a href="service-details.html">Consultorio Externo</a></h4>
                            <p class="text-justify">
                                Para una atención integral a los pacientes que presenten dolencias y que acceden a esta para obtener diferentes tipos de diagnósticos.
                            </p>
                            <a href="service-details.html" class="box-btn">Ver equipos <i class="far fa-plus"></i></a>
                            <div class="box-icon">
                                <i class="flaticon-virus"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="fancy-content-box mt-30">
                        <div class="box-thumbnail">
                            <img src="{{ asset('img/fancy-box/hadrival-cuidados-intensivos.png') }}" alt="Image">
                        </div>
                        <div class="box-content">
                            <h4 class="title"><a href="service-details.html">Cuidados Intensivos</a></h4>
                            <p class="text-justify">
                                Para realizar atenciones médicas oportunas a las personas que tienen lesiones y enfermedades que pueden ser mortales.
                            </p>
                            <a href="service-details.html" class="box-btn">Ver equipos <i class="far fa-plus"></i></a>
                            <div class="box-icon">
                                <i class="flaticon-virus"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="fancy-content-box mt-30">
                        <div class="box-thumbnail">
                            <img src="{{ asset('img/fancy-box/hadrival-gineco-obstetricia.png') }}" alt="Image">
                        </div>
                        <div class="box-content">
                            <h4 class="title"><a href="service-details.html">Gineco-Obstetricia</a></h4>
                            <p class="text-justify">
                                Realizar atenciones a mujeres durante el embarazo y el parto, y diagnóstico o tratamiento de enfermedades del sistema reproductor femenino.
                            </p>
                            <a href="service-details.html" class="box-btn">Ver equipos <i class="far fa-plus"></i></a>
                            <div class="box-icon">
                                <i class="flaticon-virus"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="fancy-content-box mt-30">
                        <div class="box-thumbnail">
                            <img src="{{ asset('img/fancy-box/hadrival-salud-ocupacional.png') }}" alt="Image">
                        </div>
                        <div class="box-content">
                            <h4 class="title"><a href="service-details.html">Salud Ocupacional</a></h4>
                            <p class="text-justify">
                                Para promover y mantener el más alto grado posible de bienestar físico, mental y social de los trabajadores en sus puestos de trabajo.
                            </p>
                            <a href="service-details.html" class="box-btn">Ver equipos <i class="far fa-plus"></i></a>
                            <div class="box-icon">
                                <i class="flaticon-virus"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="fancy-content-box mt-30">
                        <div class="box-thumbnail">
                            <img src="{{ asset('img/fancy-box/hadrival-urologia.png') }}" alt="Image">
                        </div>
                        <div class="box-content">
                            <h4 class="title"><a href="service-details.html">Urología</a></h4>
                            <p class="text-justify">
                                Para una prevención, diagnóstico y tratamiento de enfermedades morfológicas renales, de las del aparato urinario y retro-peritoneo.
                            </p>
                            <a href="service-details.html" class="box-btn">Ver equipos <i class="far fa-plus"></i></a>
                            <div class="box-icon">
                                <i class="flaticon-virus"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== Service Section End ======-->

    <!--====== Doctor Section Start ======-->
    <!--<section class="doctors-section section-gap">
        <div class="container">
            <div class="row justify-content-between align-items-center mb-70">
                <div class="col-lg-5 col-md-6">
                    <div class="section-heading">
                        <span class="tagline">Professional Doctors</span>
                        <h2 class="title">Meet Our Experience  Doctors</h2>
                    </div>
                </div>
                <div class="col-auto">
                    <a href="doctors.html" class="template-btn template-btn-primary mt-sm-30">
                        Make An Appointment <i class="far fa=plus"></i>
                    </a>
                </div>
            </div>
            <div class="doctor-bordered-wrapper">
                <div class="row doctors-loop justify-content-center">
                    <div class="col-lg-3 col-sm-6">
                        <div class="doctor-box-two mb-40 wow fadeInUp" data-wow-delay="0.3s">
                            <div class="doctor-photo">
                                <img src="assets/img/doctors/14.jpg" alt="Image">

                                <ul class="social-links">
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                                </ul>
                                <span class="plus-icon"><i class="far fa-plus"></i></span>
                            </div>
                            <div class="doctor-information">
                                <h5 class="name">
                                    <a href="doctor-details.html">Lee S. Williamson</a>
                                </h5>
                                <span class="specialty">Cardiology</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="doctor-box-two mb-40 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="doctor-photo">
                                <img src="assets/img/doctors/15.jpg" alt="Image">

                                <ul class="social-links">
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                                </ul>
                                <span class="plus-icon"><i class="far fa-plus"></i></span>
                            </div>
                            <div class="doctor-information">
                                <h5 class="name">
                                    <a href="doctor-details.html">Greg S. Grinstead </a>
                                </h5>
                                <span class="specialty">Neurology</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="doctor-box-two mb-40 wow fadeInUp" data-wow-delay="0.5s">
                            <div class="doctor-photo">
                                <img src="assets/img/doctors/16.jpg" alt="Image">

                                <ul class="social-links">
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                                </ul>
                                <span class="plus-icon"><i class="far fa-plus"></i></span>
                            </div>
                            <div class="doctor-information">
                                <h5 class="name">
                                    <a href="doctor-details.html">Roger K. Jackson </a>
                                </h5>
                                <span class="specialty">Orthopedics</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="doctor-box-two mb-40 wow fadeInUp" data-wow-delay="0.6s">
                            <div class="doctor-photo">
                                <img src="assets/img/doctors/17.jpg" alt="Image">

                                <ul class="social-links">
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                                </ul>
                                <span class="plus-icon"><i class="far fa-plus"></i></span>
                            </div>
                            <div class="doctor-information">
                                <h5 class="name">
                                    <a href="doctor-details.html">Ray. M. Drake </a>
                                </h5>
                                <span class="specialty">Cardiology</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>-->
    <!--====== Doctor Section End ======-->

    <!--====== Appointment Section Start ======-->
    <!--<section class="appointment-section">
        <div class="container container-1500">
            <div class="appointment-form-two style-two">
                <div class="appointment-image" style="background-image: url(assets/img/appointment/03.jpg);">
                </div>
                <div class="form-wrap">
                    <div class="section-heading mb-50">
                        <span class="tagline">Make an Appointment</span>
                        <h2 class="title">Fill-up The From to Get Our Medical Services </h2>
                    </div>
                    <form action="#">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-field wow fadeInLeft" data-wow-delay="0.3s">
                                    <input type="text" placeholder="Your Full Name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-field wow fadeInRight" data-wow-delay="0.4s">
                                    <input type="email" placeholder="Email Address">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-field wow fadeInLeft" data-wow-delay="0.5s">
                                    <select>
                                        <option value="1" selected disabled>Choose Doctors</option>
                                        <option value="2">Doctor One</option>
                                        <option value="3">Doctor Two</option>
                                        <option value="4">Doctor Three</option>
                                        <option value="5">Doctor Four</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-field wow fadeInRight" data-wow-delay="0.6s">
                                    <input type="date">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-field wow fadeInLeft" data-wow-delay="0.7s">
                                    <select>
                                        <option value="1" selected disabled>Services Category</option>
                                        <option value="2">Service One</option>
                                        <option value="3">Service Two</option>
                                        <option value="4">Service Three</option>
                                        <option value="5">Service Four</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-field wow fadeInRight" data-wow-delay="0.8s">
                                    <button type="submit" class="template-btn">
                                        Make an Appointment <i class="far fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>-->
    <!--====== Appointment Section End ======-->

    <!--====== Latest Blog Start ======-->
    <!--<section class="latest-blog-section section-gap">
        <div class="container">
            <div class="row justify-content-between align-items-center mb-40">
                <div class="col-lg-6 col-md-7">
                    <div class="section-heading">
                        <span class="text-uppercase tagline">Últimas noticias</span>
                        <h2 class="title">Soluciones tecnológicas de equipamiento médico</h2>
                    </div>
                </div>
                <div class="col-auto">
                    <a href="doctors.html" class="template-btn mt-sm-30">
                        View More News <i class="far fa-plus"></i>
                    </a>
                </div>
            </div>
            <div class="row justify-content-center latest-blog-loop">
                <div class="col-lg-4 col-md-6 col-sm-10">
                    <div class="latest-blog-two mt-30">
                        <div class="thumbnail">
                            <img src="assets/img/latest-blog/04.jpg" alt="Image">
                        </div>
                        <div class="blog-content">
                            <div class="blog-meta">
                                <a href="#" class="blog-category">Health</a>
                                <a href="#" class="blog-date"><i class="far fa-calendar-alt"></i> 25 Aug 2021</a>
                            </div>
                            <h4 class="blog-title"><a href="blog-details.html">Comprehensive Was Health Built Complete Guide</a></h4>
                            <div class="blog-footer">
                                <a href="#"><i class="far fa-user-circle"></i> <span>By</span> Jose S. Mahon</a>
                                <a href="#"><i class="far fa-heart"></i> <span>Like</span>(5k)</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-10">
                    <div class="latest-blog-two mt-30">
                        <div class="thumbnail">
                            <img src="assets/img/latest-blog/05.jpg" alt="Image">
                        </div>
                        <div class="blog-content">
                            <div class="blog-meta">
                                <a href="#" class="blog-category">Health</a>
                                <a href="#" class="blog-date"><i class="far fa-calendar-alt"></i> 25 Aug 2021</a>
                            </div>
                            <h4 class="blog-title"><a href="blog-details.html">Why Content Such Fmental Design Process Compre</a></h4>
                            <div class="blog-footer">
                                <a href="#"><i class="far fa-user-circle"></i> <span>By</span> Jose S. Mahon</a>
                                <a href="#"><i class="far fa-heart"></i> <span>Like</span>(5k)</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-10">
                    <div class="latest-blog-two mt-30">
                        <div class="thumbnail">
                            <img src="assets/img/latest-blog/06.jpg" alt="Image">
                        </div>
                        <div class="blog-content">
                            <div class="blog-meta">
                                <a href="#" class="blog-category">Health</a>
                                <a href="#" class="blog-date"><i class="far fa-calendar-alt"></i> 25 Aug 2021</a>
                            </div>
                            <h4 class="blog-title"><a href="blog-details.html">Comprehensive Was Health Built Complete Guide</a></h4>
                            <div class="blog-footer">
                                <a href="#"><i class="far fa-user-circle"></i> <span>By</span> Jose S. Mahon</a>
                                <a href="#"><i class="far fa-heart"></i> <span>Like</span>(5k)</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>-->
    <!--====== Latest Blog End ======-->

    <!--====== Testimonials Section Start ======-->
    <!--<section class="testimonial-section testimonial-shapes section-gap bg-color-primary polygon-pattern">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-heading text-center heading-white mb-70">
                        <span class="text-uppercase tagline">Nuestros Testimonios</span>
                        <h2 class="title">Los clientes sobre nuestros servicios</h2>
                    </div>
                </div>
            </div>
            <div class="row testimonial-slider-two">
                <div class="col-lg-6">
                    <div class="single-testimonial-slider">
                        <p class="content">
                            On the other hand we denounce riteous indignation and dislike men wh beguiled and demoralized by the charms of plsure of the moment, so blinded by desire that they cannot.
                        </p>

                        <div class="author-info-wrapper">
                            <div class="avatar">
                                <img src="assets/img/testimonial/01.png" alt="Image">
                            </div>
                            <div class="author-info">
                                <h5 class="name">Mark E. Kaminsky</h5>
                                <span class="title">Web Designer</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-testimonial-slider">
                        <p class="content">
                            On the other hand we denounce riteous indignation and dislike men wh beguiled and demoralized by the charms of plsure of the moment, so blinded by desire that they cannot.
                        </p>

                        <div class="author-info-wrapper">
                            <div class="avatar">
                                <img src="assets/img/testimonial/02.png" alt="Image">
                            </div>
                            <div class="author-info">
                                <h5 class="name">Ronald C. Kendall</h5>
                                <span class="title">Web Designer</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-testimonial-slider">
                        <p class="content">
                            On the other hand we denounce riteous indignation and dislike men wh beguiled and demoralized by the charms of plsure of the moment, so blinded by desire that they cannot.
                        </p>

                        <div class="author-info-wrapper">
                            <div class="avatar">
                                <img src="assets/img/testimonial/01.png" alt="Image">
                            </div>
                            <div class="author-info">
                                <h5 class="name">Mark E. Kaminsky</h5>
                                <span class="title">Web Designer</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-testimonial-slider">
                        <p class="content">
                            On the other hand we denounce riteous indignation and dislike men wh beguiled and demoralized by the charms of plsure of the moment, so blinded by desire that they cannot.
                        </p>

                        <div class="author-info-wrapper">
                            <div class="avatar">
                                <img src="assets/img/testimonial/02.png" alt="Image">
                            </div>
                            <div class="author-info">
                                <h5 class="name">Ronald C. Kendall</h5>
                                <span class="title">Web Designer</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="testimonial-slider-arrow"></div>
        </div>
        <div class="shape-images">
            <div class="image-one animate-float-bob-y" style="background-image: url(assets/img/testimonial/bg-1.jpg);">
            </div>
            <div class="image-two animate-float-bob-y" style="background-image: url(assets/img/testimonial/bg.jpg);">
            </div>
        </div>
    </section>-->
    <!--====== Testimonials Section End ======-->

    <!--====== Partner Section Start ======-->
    <div class="partner-section section-gap-80 bg-color-grey dots-map-pattern">
        <div class="container">
            <div class="section-heading text-center mb-50">
                <span class="text-uppercase tagline">Nuestros clientes</span>
                <!--<h2 class="title">Más de 10 entidades confiaron en nosotros</h2>-->
            </div>
            <div class="row partner-logo-slider">
                @foreach($clients as $client)
                    <div class="col partner-logo-box">
                        <a href="{{ $client->url }}" target="_blank">
                            <img src="{{ asset($client->logo) }}" alt="{{ $client->title }}">
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <!--====== Partner Section End ======-->
@endsection
